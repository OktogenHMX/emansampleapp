package com.oktogen.emansampleapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.oktogen.emansampleapp.R;
import com.oktogen.emansampleapp.api.NtApi;
import com.oktogen.emansampleapp.base.Constants;
import com.oktogen.emansampleapp.fragments.ItemDetailFragment;
import com.oktogen.emansampleapp.fragments.ItemListFragment;
import com.oktogen.emansampleapp.model.Root;
import com.oktogen.emansampleapp.provider.DataProvider;
import com.oktogen.emansampleapp.provider.DataProviderClient;
import com.oktogen.emansampleapp.utils.ConnectionDetector;
import com.oktogen.emansampleapp.utils.Logg;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ItemListActivity extends FragmentActivity implements ItemListFragment.Callbacks {
    private final String TAG = "ItemListActivity";

    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;

            ((ItemListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.item_list))
                    .setActivateOnItemClick(true);
        }
    }

    public void downloadData(int limit, int offset){

        if(ConnectionDetector.isConnected(ItemListActivity.this)){
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constants.baseUrl).build();
            NtApi ntApi = restAdapter.create(NtApi.class);

            ntApi.getTaskDetail(String.valueOf(limit), String.valueOf(offset), Constants.apiKey, new Callback<Root>() {

                @Override
                public void success(Root root, Response response) {
                    DataProviderClient.bulkInsertAppData(DataProvider.EVENTS_URI, root.getContentValues(), getApplication());
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(ItemListActivity.this, getString(R.string.dialog_text_download_failed), Toast.LENGTH_SHORT).show();
                    showTryAgainButton();
                }
            });

        }else{
            Toast.makeText(ItemListActivity.this, getString(R.string.dialog_text_no_connection), Toast.LENGTH_SHORT).show();
            showTryAgainButton();
        }
    }

    private void showTryAgainButton(){
        Fragment frag = getSupportFragmentManager().findFragmentById(R.id.item_list);

        if(frag != null){
            if(frag instanceof ItemListFragment){
                ItemListFragment listFragment = (ItemListFragment) frag;
                listFragment.showTryAgainButton();
            }
        }
    }


    @Override
    public void onItemSelected(String id) {
        Logg.v(TAG, "selected item id: "+id);

        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putString(ItemDetailFragment.ARG_ITEM_ID, id);
            ItemDetailFragment fragment = new ItemDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();

        } else {
            Intent detailIntent = new Intent(this, ItemDetailActivity.class);
            detailIntent.putExtra(ItemDetailFragment.ARG_ITEM_ID, id);
            startActivity(detailIntent);
        }
    }
}
