package com.oktogen.emansampleapp.api;

import com.oktogen.emansampleapp.base.Constants;
import com.oktogen.emansampleapp.model.Root;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by oktogen on 4.7.15.
 */
public interface NtApi {
    /*GET LIST*/
    @GET("/listings.json?")
    public void getTaskDetail(@Query(Constants.paramLimit) String limit,@Query(Constants.paramOffset) String offset, @Query(Constants.paramApiKey) String apiKey, Callback<Root> callback);   //this is an example of response POJO - make sure your variable name is the same with your json tagging
}
