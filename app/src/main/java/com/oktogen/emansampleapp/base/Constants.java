package com.oktogen.emansampleapp.base;

/**
 * Created by oktogen on 4.7.15.
 */
public class Constants {
    private static final String TAG = "Constants";

    public static final boolean isLoggEnabled = true;

    public static final String apiKey = "bac887829e791e67ba9845d622582667:13:72434162";

    public static final String baseUrl = "http://api.nytimes.com/svc/events/v2";

    public static final String paramLimit = "limit";
    public static final String paramOffset = "offset";
    public static final String paramApiKey = "api-key";

    public static final int LOADER_ID_LIST = 1;
    public static final int LOADER_ID_DETAIL = 2;
}
