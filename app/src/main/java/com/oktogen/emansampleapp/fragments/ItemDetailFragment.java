package com.oktogen.emansampleapp.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.oktogen.emansampleapp.R;
import com.oktogen.emansampleapp.activities.ItemDetailActivity;
import com.oktogen.emansampleapp.activities.ItemListActivity;
import com.oktogen.emansampleapp.base.Constants;
import com.oktogen.emansampleapp.provider.DataProvider;
import com.oktogen.emansampleapp.utils.Logg;

public class ItemDetailFragment extends Fragment  implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final String TAG = "ItemDetailFragment";
    public static final String ARG_ITEM_ID = "arg_item_id";
    private TextView detailTextView = null;
    private String content = null;

    public ItemDetailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_detail, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        detailTextView = ((TextView) getView().findViewById(R.id.item_detail));

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            Loader loader = getActivity().getSupportLoaderManager().getLoader(Constants.LOADER_ID_DETAIL);

            if(loader == null){
                getActivity().getSupportLoaderManager().initLoader(Constants.LOADER_ID_DETAIL, null, this);
            }else{
                getActivity().getSupportLoaderManager().restartLoader(Constants.LOADER_ID_DETAIL, null, this);
            }
        }

        /*if(content !=  null){
            detailTextView.setText(Html.fromHtml(content));
        }*/
    }

    private void setActionBarTitle(String title){
        if(getActivity() instanceof ItemDetailActivity){
            ItemDetailActivity itemDetailActivity = (ItemDetailActivity) getActivity();
            itemDetailActivity.getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = DataProvider.EVENTS_EVENTID_COLUMN+" = ?";
        String[] selectionArgs = {getArguments().getString(ARG_ITEM_ID)};

        return new CursorLoader(getActivity(), DataProvider.EVENTS_URI, null, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            if(data.getCount() > 0){
                data.moveToFirst();
                content = data.getString(data.getColumnIndexOrThrow(DataProvider.EVENTS_WEBDESCRIPTION_COLUMN));
                if(detailTextView != null){
                    detailTextView.setText(Html.fromHtml(content));
                }
                setActionBarTitle(data.getString(data.getColumnIndexOrThrow(DataProvider.EVENTS_EVENTNAME_COLUMN)));
            }
        }

        if(data != null){
            for(int i=0; i<data.getColumnNames().length; i++){
                //Logg.v(TAG, data.getColumnNames()[i]);
            }
        }else{
            Logg.w(TAG, "cursor is null");
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}