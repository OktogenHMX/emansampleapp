package com.oktogen.emansampleapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ListView;
import android.widget.TextView;


import com.commonsware.cwac.endless.EndlessAdapter;
import com.oktogen.emansampleapp.R;
import com.oktogen.emansampleapp.activities.ItemListActivity;
import com.oktogen.emansampleapp.provider.DataProvider;
import com.oktogen.emansampleapp.utils.Logg;

public class ItemListFragment extends ListFragment  implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final String TAG = "ItemListFragment";
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    private Callbacks mCallbacks = callbacks;
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private CustomEndlessAdapter endlessAdapter;
    private NewsCursorAdapter newsCursorAapter = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        getActivity().getSupportLoaderManager().initLoader(1, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = callbacks;
    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), DataProvider.EVENTS_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(endlessAdapter != null){
            newsCursorAapter.changeCursor(data);
            endlessAdapter.onDataReady();
        }else{
            newsCursorAapter = new NewsCursorAdapter(getActivity(), data);
            endlessAdapter = new CustomEndlessAdapter(getActivity(), newsCursorAapter) {
            };

            endlessAdapter.setRunInBackground(false);
            setListAdapter(endlessAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public interface Callbacks {
        void onItemSelected(String id);
    }

    private static Callbacks callbacks = new Callbacks() {
        @Override
        public void onItemSelected(String id) {
        }
    };

    public ItemListFragment() {

    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        Cursor data = (Cursor)getListAdapter().getItem(position);
        mCallbacks.onItemSelected(String.valueOf(data.getString(data.getColumnIndexOrThrow(DataProvider.EVENTS_EVENTID_COLUMN))));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    public void tryAgain(){
        endlessAdapter.onDataReady();
    }

    public void showTryAgainButton(){
        endlessAdapter.showButton();
    }

    public class NewsCursorAdapter extends CursorAdapter {
        public NewsCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(R.layout.row, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView tvBody = (TextView) view.findViewById(R.id.text1);
            String title = cursor.getString(cursor.getColumnIndexOrThrow(DataProvider.EVENTS_EVENTNAME_COLUMN));
            tvBody.setText(title);

            TextView tv2 = (TextView) view.findViewById(R.id.text2);
            String st1 = cursor.getString(cursor.getColumnIndexOrThrow(DataProvider.EVENTS_CRITICNAME_COLUMN));
            tv2.setText(st1);

        }
    }

    class CustomEndlessAdapter extends EndlessAdapter implements View.OnClickListener{
        private RotateAnimation rotate=null;
        private View throbberView = null;
        private View pendingView = null;

        CustomEndlessAdapter(Context ctx, CursorAdapter cursorAdapter) {
            super(cursorAdapter);

            rotate=new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                    0.5f, Animation.RELATIVE_TO_SELF,
                    0.5f);
            rotate.setDuration(600);
            rotate.setRepeatMode(Animation.RESTART);
            rotate.setRepeatCount(Animation.INFINITE);
        }

        @Override
        protected View getPendingView(ViewGroup parent) {
            View row=LayoutInflater.from(parent.getContext()).inflate(R.layout.row, null);
            pendingView = row;

            throbberView =row.findViewById(R.id.text_container);
            throbberView.setVisibility(View.GONE);
            throbberView =row.findViewById(R.id.try_again_button);
            setListener(throbberView);
            throbberView.setVisibility(View.GONE);
            throbberView =row.findViewById(R.id.throbber);
            throbberView.setVisibility(View.VISIBLE);
            startProgressAnimation();
            ItemListActivity itemListActivity = (ItemListActivity)getActivity();
            itemListActivity.downloadData(5, endlessAdapter.getCount() + 4);
            Logg.v(TAG, "getPendingView");
            return(row);
        }

        @Override
        protected boolean cacheInBackground() {
            Logg.v(TAG, "cacheInBackground");
            return(getWrappedAdapter().getCount()<1000);
        }

        @Override
        protected void appendCachedData() {
            Logg.v(TAG, "appendCachedData");
        }

        private void setListener(View view){
            view.setOnClickListener(this);
        }

        public void showButton(){
            if(pendingView != null){
                throbberView =pendingView.findViewById(R.id.text_container);
                throbberView.setVisibility(View.GONE);
                throbberView =pendingView.findViewById(R.id.try_again_button);
                throbberView.setVisibility(View.VISIBLE);
                throbberView =pendingView.findViewById(R.id.throbber);
                throbberView.setVisibility(View.GONE);
            }
        }

        void startProgressAnimation() {
            if (throbberView !=null) {
                throbberView.startAnimation(rotate);
            }
        }

        @Override
        public void onClick(View v) {
            tryAgain();
        }
    }
}