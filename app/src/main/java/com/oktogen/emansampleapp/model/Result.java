package com.oktogen.emansampleapp.model;

/**
 * Created by oktogen on 4.7.15.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("event_id")
    @Expose
    private Integer eventId;
    @SerializedName("event_schedule_id")
    @Expose
    private Integer eventScheduleId;
    @SerializedName("last_modified")
    @Expose
    private String lastModified;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_detail_url")
    @Expose
    private String eventDetailUrl;
    @SerializedName("web_description")
    @Expose
    private String webDescription;
    @Expose
    private String city;
    @Expose
    private String state;
    @SerializedName("film_rating")
    @Expose
    private Boolean filmRating;
    @SerializedName("critic_name")
    @Expose
    private String criticName;
    @Expose
    private String category;
    @SerializedName("times_pick")
    @Expose
    private Boolean timesPick;
    @Expose
    private Boolean free;
    @SerializedName("kid_friendly")
    @Expose
    private Boolean kidFriendly;
    @SerializedName("last_chance")
    @Expose
    private Boolean lastChance;
    @Expose
    private Boolean festival;
    @SerializedName("long_running_show")
    @Expose
    private Boolean longRunningShow;
    @SerializedName("previews_and_openings")
    @Expose
    private Boolean previewsAndOpenings;
    @SerializedName("recurring_start_date")
    @Expose
    private String recurringStartDate;
    @SerializedName("recur_days")
    @Expose
    private List<String> recurDays = new ArrayList<String>();

    /**
     *
     * @return
     * The eventId
     */
    public Integer getEventId() {
        return eventId;
    }

    /**
     *
     * @param eventId
     * The event_id
     */
    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    /**
     *
     * @return
     * The eventScheduleId
     */
    public Integer getEventScheduleId() {
        return eventScheduleId;
    }

    /**
     *
     * @param eventScheduleId
     * The event_schedule_id
     */
    public void setEventScheduleId(Integer eventScheduleId) {
        this.eventScheduleId = eventScheduleId;
    }

    /**
     *
     * @return
     * The lastModified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     *
     * @param lastModified
     * The last_modified
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     *
     * @return
     * The eventName
     */
    public String getEventName() {

        if(this.eventName.startsWith("‘")){
            eventName = eventName.substring(1, eventName.length()-1);
        }

        if(this.eventName.endsWith("’")){
            eventName = eventName.substring(0, eventName.length()-2);
        }
        return this.eventName;
    }

    /**
     *
     * @param eventName
     * The event_name
     */
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    /**
     *
     * @return
     * The eventDetailUrl
     */
    public String getEventDetailUrl() {
        return eventDetailUrl;
    }

    /**
     *
     * @param eventDetailUrl
     * The event_detail_url
     */
    public void setEventDetailUrl(String eventDetailUrl) {
        this.eventDetailUrl = eventDetailUrl;
    }

    /**
     *
     * @return
     * The webDescription
     */
    public String getWebDescription() {
        return webDescription;
    }

    /**
     *
     * @param webDescription
     * The web_description
     */
    public void setWebDescription(String webDescription) {
        this.webDescription = webDescription;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The filmRating
     */
    public Boolean getFilmRating() {
        return filmRating;
    }

    /**
     *
     * @param filmRating
     * The film_rating
     */
    public void setFilmRating(Boolean filmRating) {
        this.filmRating = filmRating;
    }

    /**
     *
     * @return
     * The criticName
     */
    public String getCriticName() {
        return criticName;
    }

    /**
     *
     * @param criticName
     * The critic_name
     */
    public void setCriticName(String criticName) {
        this.criticName = criticName;
    }

    /**
     *
     * @return
     * The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     * The timesPick
     */
    public Boolean getTimesPick() {
        return timesPick;
    }

    /**
     *
     * @param timesPick
     * The times_pick
     */
    public void setTimesPick(Boolean timesPick) {
        this.timesPick = timesPick;
    }

    /**
     *
     * @return
     * The free
     */
    public Boolean getFree() {
        return free;
    }

    /**
     *
     * @param free
     * The free
     */
    public void setFree(Boolean free) {
        this.free = free;
    }

    /**
     *
     * @return
     * The kidFriendly
     */
    public Boolean getKidFriendly() {
        return kidFriendly;
    }

    /**
     *
     * @param kidFriendly
     * The kid_friendly
     */
    public void setKidFriendly(Boolean kidFriendly) {
        this.kidFriendly = kidFriendly;
    }

    /**
     *
     * @return
     * The lastChance
     */
    public Boolean getLastChance() {
        return lastChance;
    }

    /**
     *
     * @param lastChance
     * The last_chance
     */
    public void setLastChance(Boolean lastChance) {
        this.lastChance = lastChance;
    }

    /**
     *
     * @return
     * The festival
     */
    public Boolean getFestival() {
        return festival;
    }

    /**
     *
     * @param festival
     * The festival
     */
    public void setFestival(Boolean festival) {
        this.festival = festival;
    }

    /**
     *
     * @return
     * The longRunningShow
     */
    public Boolean getLongRunningShow() {
        return longRunningShow;
    }

    /**
     *
     * @param longRunningShow
     * The long_running_show
     */
    public void setLongRunningShow(Boolean longRunningShow) {
        this.longRunningShow = longRunningShow;
    }

    /**
     *
     * @return
     * The previewsAndOpenings
     */
    public Boolean getPreviewsAndOpenings() {
        return previewsAndOpenings;
    }

    /**
     *
     * @param previewsAndOpenings
     * The previews_and_openings
     */
    public void setPreviewsAndOpenings(Boolean previewsAndOpenings) {
        this.previewsAndOpenings = previewsAndOpenings;
    }

    /**
     *
     * @return
     * The recurringStartDate
     */
    public String getRecurringStartDate() {
        return recurringStartDate;
    }

    /**
     *
     * @param recurringStartDate
     * The recurring_start_date
     */
    public void setRecurringStartDate(String recurringStartDate) {
        this.recurringStartDate = recurringStartDate;
    }

    /**
     *
     * @return
     * The recurDays
     */
    public List<String> getRecurDays() {
        return recurDays;
    }

    /**
     *
     * @param recurDays
     * The recur_days
     */
    public void setRecurDays(List<String> recurDays) {
        this.recurDays = recurDays;
    }

}
