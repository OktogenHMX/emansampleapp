package com.oktogen.emansampleapp.model;

/**
 * Created by oktogen on 4.7.15.
 */

import android.content.ContentValues;

import java.util.List;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.oktogen.emansampleapp.provider.DataProvider;


public class Root {

    @Expose
    private String status;
    @Expose
    private String copyright;
    @SerializedName("num_results")
    @Expose
    private Integer numResults;
    @Expose
    private List<Result> results = new ArrayList<Result>();

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The copyright
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     *
     * @param copyright
     * The copyright
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     *
     * @return
     * The numResults
     */
    public Integer getNumResults() {
        return numResults;
    }

    /**
     *
     * @param numResults
     * The num_results
     */
    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }

    /**
     *
     * @return
     * The results
     */
    public List<Result> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<Result> results) {
        this.results = results;
    }

    public ContentValues[] getContentValues() {
        List<ContentValues> cvList = new ArrayList<ContentValues>();

        for(int i=0; i<results.size(); i++){
            Result result = results.get(i);
            if(result != null){
                ContentValues contentValues = new ContentValues();
                contentValues.put(DataProvider.EVENTS_CATEGORY_COLUMN, result.getCategory());
                contentValues.put(DataProvider.EVENTS_CRITICNAME_COLUMN, result.getCriticName());
                contentValues.put(DataProvider.EVENTS_EVENTDETAILURL_COLUMN, result.getEventDetailUrl());
                contentValues.put(DataProvider.EVENTS_EVENTID_COLUMN, result.getEventId());
                contentValues.put(DataProvider.EVENTS_EVENTNAME_COLUMN, result.getEventName());
                contentValues.put(DataProvider.EVENTS_EVENTSCHEDULEID_COLUMN, result.getEventScheduleId());
                contentValues.put(DataProvider.EVENTS_FESTIVAL_COLUMN, result.getFestival());
                contentValues.put(DataProvider.EVENTS_FILMRATING_COLUMN, result.getFilmRating());
                contentValues.put(DataProvider.EVENTS_FREE_COLUMN, result.getFree());
                contentValues.put(DataProvider.EVENTS_CRITICNAME_COLUMN, result.getCriticName());
                contentValues.put(DataProvider.EVENTS_EVENTDETAILURL_COLUMN, result.getEventDetailUrl());
                contentValues.put(DataProvider.EVENTS_KIDFRIENDLY_COLUMN, result.getKidFriendly());
                contentValues.put(DataProvider.EVENTS_LASTCHANCE_COLUMN, result.getLastChance());
                contentValues.put(DataProvider.EVENTS_WEBDESCRIPTION_COLUMN, result.getWebDescription());
                contentValues.put(DataProvider.EVENTS_TIMESPICK_COLUMN, result.getTimesPick());
                contentValues.put(DataProvider.EVENTS_STATE_COLUMN, result.getState());
                contentValues.put(DataProvider.EVENTS_RECURRINGSTARTDATE_COLUMN, result.getRecurringStartDate());
                contentValues.put(DataProvider.EVENTS_PREVIEWSANDOPENINGS_COLUMN, result.getPreviewsAndOpenings());
                contentValues.put(DataProvider.EVENTS_LONGRUNNINGSHOW_COLUMN, result.getLongRunningShow());
                contentValues.put(DataProvider.EVENTS_LASTMODIFIED_COLUMN, result.getLastModified());
                cvList.add(contentValues);
            }
        }

        ContentValues[] cvArr = new ContentValues[cvList.size()];

        for(int ia=0; ia<cvList.size(); ia++){
            cvArr[ia] = cvList.get(ia);
        }

        return cvArr;
    }
}