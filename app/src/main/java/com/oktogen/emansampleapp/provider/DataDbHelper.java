/**********************************************************************************************************************************************************************
****** AUTO GENERATED FILE BY ANDROID SQLITE HELPER SCRIPT BY FEDERICO PAOLINELLI. ANY CHANGE WILL BE WIPED OUT IF THE SCRIPT IS PROCESSED AGAIN. *******
**********************************************************************************************************************************************************************/
package com.oktogen.emansampleapp.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import java.util.Date;

public class DataDbHelper {
    private static final String TAG = "Data";

    private static final String DATABASE_NAME = "Data.db";
    private static final int DATABASE_VERSION = 1;


    // Variable to hold the database instance
    protected SQLiteDatabase mDb;
    // Context of the application using the database.
    private final Context mContext;
    // Database open/upgrade helper
    private DbHelper mDbHelper;
    
    public DataDbHelper(Context context) {
        mContext = context;
        mDbHelper = new DbHelper(mContext, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    public DataDbHelper open() throws SQLException { 
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }
                                                     
    public void close() {
        mDb.close();
    }

    public static final String ROW_ID = "_id";

    
    // -------------- EVENTS DEFINITIONS ------------
    public static final String EVENTS_TABLE = "Events";
    
    public static final String EVENTS_EVENTID_COLUMN = "eventId";
    public static final int EVENTS_EVENTID_COLUMN_POSITION = 1;
    
    public static final String EVENTS_EVENTSCHEDULEID_COLUMN = "eventScheduleId";
    public static final int EVENTS_EVENTSCHEDULEID_COLUMN_POSITION = 2;
    
    public static final String EVENTS_LASTMODIFIED_COLUMN = "lastModified";
    public static final int EVENTS_LASTMODIFIED_COLUMN_POSITION = 3;
    
    public static final String EVENTS_EVENTNAME_COLUMN = "eventName";
    public static final int EVENTS_EVENTNAME_COLUMN_POSITION = 4;
    
    public static final String EVENTS_EVENTDETAILURL_COLUMN = "eventDetailUrl";
    public static final int EVENTS_EVENTDETAILURL_COLUMN_POSITION = 5;
    
    public static final String EVENTS_WEBDESCRIPTION_COLUMN = "webDescription";
    public static final int EVENTS_WEBDESCRIPTION_COLUMN_POSITION = 6;
    
    public static final String EVENTS_STATE_COLUMN = "state";
    public static final int EVENTS_STATE_COLUMN_POSITION = 7;
    
    public static final String EVENTS_FILMRATING_COLUMN = "filmRating";
    public static final int EVENTS_FILMRATING_COLUMN_POSITION = 8;
    
    public static final String EVENTS_CRITICNAME_COLUMN = "criticName";
    public static final int EVENTS_CRITICNAME_COLUMN_POSITION = 9;
    
    public static final String EVENTS_CATEGORY_COLUMN = "category";
    public static final int EVENTS_CATEGORY_COLUMN_POSITION = 10;
    
    public static final String EVENTS_TIMESPICK_COLUMN = "timesPick";
    public static final int EVENTS_TIMESPICK_COLUMN_POSITION = 11;
    
    public static final String EVENTS_FREE_COLUMN = "free";
    public static final int EVENTS_FREE_COLUMN_POSITION = 12;
    
    public static final String EVENTS_KIDFRIENDLY_COLUMN = "kidFriendly";
    public static final int EVENTS_KIDFRIENDLY_COLUMN_POSITION = 13;
    
    public static final String EVENTS_LASTCHANCE_COLUMN = "lastChance";
    public static final int EVENTS_LASTCHANCE_COLUMN_POSITION = 14;
    
    public static final String EVENTS_FESTIVAL_COLUMN = "festival";
    public static final int EVENTS_FESTIVAL_COLUMN_POSITION = 15;
    
    public static final String EVENTS_LONGRUNNINGSHOW_COLUMN = "longRunningShow";
    public static final int EVENTS_LONGRUNNINGSHOW_COLUMN_POSITION = 16;
    
    public static final String EVENTS_PREVIEWSANDOPENINGS_COLUMN = "previewsAndOpenings";
    public static final int EVENTS_PREVIEWSANDOPENINGS_COLUMN_POSITION = 17;
    
    public static final String EVENTS_RECURRINGSTARTDATE_COLUMN = "recurringStartDate";
    public static final int EVENTS_RECURRINGSTARTDATE_COLUMN_POSITION = 18;
    
    


    // -------- TABLES CREATION ----------

    
    // Events CREATION 
    private static final String DATABASE_EVENTS_CREATE = "create table " + EVENTS_TABLE + " (" +
                                "_id integer primary key autoincrement, " +
                                EVENTS_EVENTID_COLUMN + " integer, " +
                                EVENTS_EVENTSCHEDULEID_COLUMN + " integer, " +
                                EVENTS_LASTMODIFIED_COLUMN + " text, " +
                                EVENTS_EVENTNAME_COLUMN + " text, " +
                                EVENTS_EVENTDETAILURL_COLUMN + " text, " +
                                EVENTS_WEBDESCRIPTION_COLUMN + " text, " +
                                EVENTS_STATE_COLUMN + " text, " +
                                EVENTS_FILMRATING_COLUMN + " integer, " +
                                EVENTS_CRITICNAME_COLUMN + " text, " +
                                EVENTS_CATEGORY_COLUMN + " text, " +
                                EVENTS_TIMESPICK_COLUMN + " integer, " +
                                EVENTS_FREE_COLUMN + " integer, " +
                                EVENTS_KIDFRIENDLY_COLUMN + " integer, " +
                                EVENTS_LASTCHANCE_COLUMN + " integer, " +
                                EVENTS_FESTIVAL_COLUMN + " integer, " +
                                EVENTS_LONGRUNNINGSHOW_COLUMN + " integer, " +
                                EVENTS_PREVIEWSANDOPENINGS_COLUMN + " integer, " +
                                EVENTS_RECURRINGSTARTDATE_COLUMN + " text" +
                                ")";
    

    
    // ----------------Events HELPERS -------------------- 
    public long addEvents (Integer eventId, Integer eventScheduleId, String lastModified, String eventName, String eventDetailUrl, String webDescription, String state, Integer filmRating, String criticName, String category, Integer timesPick, Integer free, Integer kidFriendly, Integer lastChance, Integer festival, Integer longRunningShow, Integer previewsAndOpenings, String recurringStartDate) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EVENTS_EVENTID_COLUMN, eventId);
        contentValues.put(EVENTS_EVENTSCHEDULEID_COLUMN, eventScheduleId);
        contentValues.put(EVENTS_LASTMODIFIED_COLUMN, lastModified);
        contentValues.put(EVENTS_EVENTNAME_COLUMN, eventName);
        contentValues.put(EVENTS_EVENTDETAILURL_COLUMN, eventDetailUrl);
        contentValues.put(EVENTS_WEBDESCRIPTION_COLUMN, webDescription);
        contentValues.put(EVENTS_STATE_COLUMN, state);
        contentValues.put(EVENTS_FILMRATING_COLUMN, filmRating);
        contentValues.put(EVENTS_CRITICNAME_COLUMN, criticName);
        contentValues.put(EVENTS_CATEGORY_COLUMN, category);
        contentValues.put(EVENTS_TIMESPICK_COLUMN, timesPick);
        contentValues.put(EVENTS_FREE_COLUMN, free);
        contentValues.put(EVENTS_KIDFRIENDLY_COLUMN, kidFriendly);
        contentValues.put(EVENTS_LASTCHANCE_COLUMN, lastChance);
        contentValues.put(EVENTS_FESTIVAL_COLUMN, festival);
        contentValues.put(EVENTS_LONGRUNNINGSHOW_COLUMN, longRunningShow);
        contentValues.put(EVENTS_PREVIEWSANDOPENINGS_COLUMN, previewsAndOpenings);
        contentValues.put(EVENTS_RECURRINGSTARTDATE_COLUMN, recurringStartDate);
        return mDb.insert(EVENTS_TABLE, null, contentValues);
    }

    public long updateEvents (long rowIndex,Integer eventId, Integer eventScheduleId, String lastModified, String eventName, String eventDetailUrl, String webDescription, String state, Integer filmRating, String criticName, String category, Integer timesPick, Integer free, Integer kidFriendly, Integer lastChance, Integer festival, Integer longRunningShow, Integer previewsAndOpenings, String recurringStartDate) {
        String where = ROW_ID + " = " + rowIndex;
        ContentValues contentValues = new ContentValues();
        contentValues.put(EVENTS_EVENTID_COLUMN, eventId);
        contentValues.put(EVENTS_EVENTSCHEDULEID_COLUMN, eventScheduleId);
        contentValues.put(EVENTS_LASTMODIFIED_COLUMN, lastModified);
        contentValues.put(EVENTS_EVENTNAME_COLUMN, eventName);
        contentValues.put(EVENTS_EVENTDETAILURL_COLUMN, eventDetailUrl);
        contentValues.put(EVENTS_WEBDESCRIPTION_COLUMN, webDescription);
        contentValues.put(EVENTS_STATE_COLUMN, state);
        contentValues.put(EVENTS_FILMRATING_COLUMN, filmRating);
        contentValues.put(EVENTS_CRITICNAME_COLUMN, criticName);
        contentValues.put(EVENTS_CATEGORY_COLUMN, category);
        contentValues.put(EVENTS_TIMESPICK_COLUMN, timesPick);
        contentValues.put(EVENTS_FREE_COLUMN, free);
        contentValues.put(EVENTS_KIDFRIENDLY_COLUMN, kidFriendly);
        contentValues.put(EVENTS_LASTCHANCE_COLUMN, lastChance);
        contentValues.put(EVENTS_FESTIVAL_COLUMN, festival);
        contentValues.put(EVENTS_LONGRUNNINGSHOW_COLUMN, longRunningShow);
        contentValues.put(EVENTS_PREVIEWSANDOPENINGS_COLUMN, previewsAndOpenings);
        contentValues.put(EVENTS_RECURRINGSTARTDATE_COLUMN, recurringStartDate);
        return mDb.update(EVENTS_TABLE, contentValues, where, null);
    }

    public boolean removeEvents(long rowIndex){
        return mDb.delete(EVENTS_TABLE, ROW_ID + " = " + rowIndex, null) > 0;
    }

    public boolean removeAllEvents(){
        return mDb.delete(EVENTS_TABLE, null, null) > 0;
    }

    public Cursor getAllEvents(){
    	return mDb.query(EVENTS_TABLE, new String[] {
                         ROW_ID,
                         EVENTS_EVENTID_COLUMN,
                         EVENTS_EVENTSCHEDULEID_COLUMN,
                         EVENTS_LASTMODIFIED_COLUMN,
                         EVENTS_EVENTNAME_COLUMN,
                         EVENTS_EVENTDETAILURL_COLUMN,
                         EVENTS_WEBDESCRIPTION_COLUMN,
                         EVENTS_STATE_COLUMN,
                         EVENTS_FILMRATING_COLUMN,
                         EVENTS_CRITICNAME_COLUMN,
                         EVENTS_CATEGORY_COLUMN,
                         EVENTS_TIMESPICK_COLUMN,
                         EVENTS_FREE_COLUMN,
                         EVENTS_KIDFRIENDLY_COLUMN,
                         EVENTS_LASTCHANCE_COLUMN,
                         EVENTS_FESTIVAL_COLUMN,
                         EVENTS_LONGRUNNINGSHOW_COLUMN,
                         EVENTS_PREVIEWSANDOPENINGS_COLUMN,
                         EVENTS_RECURRINGSTARTDATE_COLUMN
                         }, null, null, null, null, null);
    }

    public Cursor getEvents(long rowIndex) {
        Cursor res = mDb.query(EVENTS_TABLE, new String[] {
                                ROW_ID,
                                EVENTS_EVENTID_COLUMN,
                                EVENTS_EVENTSCHEDULEID_COLUMN,
                                EVENTS_LASTMODIFIED_COLUMN,
                                EVENTS_EVENTNAME_COLUMN,
                                EVENTS_EVENTDETAILURL_COLUMN,
                                EVENTS_WEBDESCRIPTION_COLUMN,
                                EVENTS_STATE_COLUMN,
                                EVENTS_FILMRATING_COLUMN,
                                EVENTS_CRITICNAME_COLUMN,
                                EVENTS_CATEGORY_COLUMN,
                                EVENTS_TIMESPICK_COLUMN,
                                EVENTS_FREE_COLUMN,
                                EVENTS_KIDFRIENDLY_COLUMN,
                                EVENTS_LASTCHANCE_COLUMN,
                                EVENTS_FESTIVAL_COLUMN,
                                EVENTS_LONGRUNNINGSHOW_COLUMN,
                                EVENTS_PREVIEWSANDOPENINGS_COLUMN,
                                EVENTS_RECURRINGSTARTDATE_COLUMN
                                }, ROW_ID + " = " + rowIndex, null, null, null, null);

        if(res != null){
            res.moveToFirst();
        }
        return res;
    }
    

    private static class DbHelper extends SQLiteOpenHelper {
        public DbHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        // Called when no database exists in disk and the helper class needs
        // to create a new one. 
        @Override
        public void onCreate(SQLiteDatabase db) {      
            db.execSQL(DATABASE_EVENTS_CREATE);
            
        }

        // Called when there is a database version mismatch meaning that the version
        // of the database on disk needs to be upgraded to the current version.
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Log the version upgrade.
            Log.w(TAG, "Upgrading from version " + 
                        oldVersion + " to " +
                        newVersion + ", which will destroy all old data");
            
            // Upgrade the existing database to conform to the new version. Multiple 
            // previous versions can be handled by comparing _oldVersion and _newVersion
            // values.

            // The simplest case is to drop the old table and create a new one.
            db.execSQL("DROP TABLE IF EXISTS " + EVENTS_TABLE + "");
            
            // Create a new one.
            onCreate(db);
        }
    }
}

