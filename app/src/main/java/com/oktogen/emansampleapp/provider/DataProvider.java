/**********************************************************************************************************************************************************************
****** AUTO GENERATED FILE BY ANDROID SQLITE HELPER SCRIPT BY FEDERICO PAOLINELLI. ANY CHANGE WILL BE WIPED OUT IF THE SCRIPT IS PROCESSED AGAIN. *******
**********************************************************************************************************************************************************************/
package com.oktogen.emansampleapp.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.oktogen.emansampleapp.utils.Logg;

import java.util.Date;

public class DataProvider extends ContentProvider {
    private static final String TAG = "DataProvider";

    protected static final String DATABASE_NAME = "Data.db";
    protected static final int DATABASE_VERSION = 1;

    // --------------- URIS --------------------
    public static final Uri EVENTS_URI = Uri.parse("content://com.oktogen.emansampleapp.provider/Events");
    
    // -------------- EVENTS DEFINITIONS ------------
    public static final String EVENTS_TABLE = "Events";

    public static final String EVENTS_EVENTID_COLUMN = "eventId";
    public static final int EVENTS_EVENTID_COLUMN_POSITION = 1;
    public static final String EVENTS_EVENTSCHEDULEID_COLUMN = "eventScheduleId";
    public static final int EVENTS_EVENTSCHEDULEID_COLUMN_POSITION = 2;
    public static final String EVENTS_LASTMODIFIED_COLUMN = "lastModified";
    public static final int EVENTS_LASTMODIFIED_COLUMN_POSITION = 3;
    public static final String EVENTS_EVENTNAME_COLUMN = "eventName";
    public static final int EVENTS_EVENTNAME_COLUMN_POSITION = 4;
    public static final String EVENTS_EVENTDETAILURL_COLUMN = "eventDetailUrl";
    public static final int EVENTS_EVENTDETAILURL_COLUMN_POSITION = 5;
    public static final String EVENTS_WEBDESCRIPTION_COLUMN = "webDescription";
    public static final int EVENTS_WEBDESCRIPTION_COLUMN_POSITION = 6;
    public static final String EVENTS_STATE_COLUMN = "state";
    public static final int EVENTS_STATE_COLUMN_POSITION = 7;
    public static final String EVENTS_FILMRATING_COLUMN = "filmRating";
    public static final int EVENTS_FILMRATING_COLUMN_POSITION = 8;
    public static final String EVENTS_CRITICNAME_COLUMN = "criticName";
    public static final int EVENTS_CRITICNAME_COLUMN_POSITION = 9;
    public static final String EVENTS_CATEGORY_COLUMN = "category";
    public static final int EVENTS_CATEGORY_COLUMN_POSITION = 10;
    public static final String EVENTS_TIMESPICK_COLUMN = "timesPick";
    public static final int EVENTS_TIMESPICK_COLUMN_POSITION = 11;
    public static final String EVENTS_FREE_COLUMN = "free";
    public static final int EVENTS_FREE_COLUMN_POSITION = 12;
    public static final String EVENTS_KIDFRIENDLY_COLUMN = "kidFriendly";
    public static final int EVENTS_KIDFRIENDLY_COLUMN_POSITION = 13;
    public static final String EVENTS_LASTCHANCE_COLUMN = "lastChance";
    public static final int EVENTS_LASTCHANCE_COLUMN_POSITION = 14;
    public static final String EVENTS_FESTIVAL_COLUMN = "festival";
    public static final int EVENTS_FESTIVAL_COLUMN_POSITION = 15;
    public static final String EVENTS_LONGRUNNINGSHOW_COLUMN = "longRunningShow";
    public static final int EVENTS_LONGRUNNINGSHOW_COLUMN_POSITION = 16;
    public static final String EVENTS_PREVIEWSANDOPENINGS_COLUMN = "previewsAndOpenings";
    public static final int EVENTS_PREVIEWSANDOPENINGS_COLUMN_POSITION = 17;
    public static final String EVENTS_RECURRINGSTARTDATE_COLUMN = "recurringStartDate";
    public static final int EVENTS_RECURRINGSTARTDATE_COLUMN_POSITION = 18;
    public static final int ALL_EVENTS = 0;
    public static final int SINGLE_EVENTS = 1;

    

    public static final String ROW_ID = "_id";

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    
        uriMatcher.addURI("com.oktogen.emansampleapp.provider", "Events", ALL_EVENTS);
        uriMatcher.addURI("com.oktogen.emansampleapp.provider", "Events/#", SINGLE_EVENTS);
    }
 

    // -------- TABLES CREATION ----------
    
    // Events CREATION 
    private static final String DATABASE_EVENTS_CREATE = "create table " + EVENTS_TABLE + " (" +
                                "_id integer primary key autoincrement, " +
                                EVENTS_EVENTID_COLUMN + " integer, " +
                                EVENTS_EVENTSCHEDULEID_COLUMN + " integer, " +
                                EVENTS_LASTMODIFIED_COLUMN + " text, " +
                                EVENTS_EVENTNAME_COLUMN + " text, " +
                                EVENTS_EVENTDETAILURL_COLUMN + " text, " +
                                EVENTS_WEBDESCRIPTION_COLUMN + " text, " +
                                EVENTS_STATE_COLUMN + " text, " +
                                EVENTS_FILMRATING_COLUMN + " integer, " +
                                EVENTS_CRITICNAME_COLUMN + " text, " +
                                EVENTS_CATEGORY_COLUMN + " text, " +
                                EVENTS_TIMESPICK_COLUMN + " integer, " +
                                EVENTS_FREE_COLUMN + " integer, " +
                                EVENTS_KIDFRIENDLY_COLUMN + " integer, " +
                                EVENTS_LASTCHANCE_COLUMN + " integer, " +
                                EVENTS_FESTIVAL_COLUMN + " integer, " +
                                EVENTS_LONGRUNNINGSHOW_COLUMN + " integer, " +
                                EVENTS_PREVIEWSANDOPENINGS_COLUMN + " integer, " +
                                EVENTS_RECURRINGSTARTDATE_COLUMN + " text" +
                                ")";
    

    protected DbHelper myOpenHelper;

    @Override
    public boolean onCreate() {
        myOpenHelper = new DbHelper(getContext(), DATABASE_NAME, null, DATABASE_VERSION);
        return true;
    }

    /**
    * Returns the right table name for the given uri
    * @param uri
    * @return
    */
    private String getTableNameFromUri(Uri uri){
        switch (uriMatcher.match(uri)) {
            case ALL_EVENTS:
            case SINGLE_EVENTS:
                return EVENTS_TABLE;
            default: break;
        }
        return null;
    }
    
    /**
    * Returns the parent uri for the given uri
    * @param uri
    * @return
    */
    private Uri getContentUriFromUri(Uri uri){
        switch (uriMatcher.match(uri)) {
            case ALL_EVENTS:
            case SINGLE_EVENTS:
                return EVENTS_URI;
            default: break;
        }
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
        String[] selectionArgs, String sortOrder) {

        // Open thedatabase.
        SQLiteDatabase db;
        try {
            db = myOpenHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = myOpenHelper.getReadableDatabase();
        }

        // Replace these with valid SQL statements if necessary.
        String groupBy = null;
        String having = null;

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // If this is a row query, limit the result set to the passed in row.
        switch (uriMatcher.match(uri)) {
            case SINGLE_EVENTS:
                String rowID = uri.getPathSegments().get(1);
                queryBuilder.appendWhere(ROW_ID + "=" + rowID);
            default: break;
        }

        // Specify the table on which to perform the query. This can
        // be a specific table or a join as required.
        queryBuilder.setTables(getTableNameFromUri(uri));

        // Execute the query.
        Cursor cursor = queryBuilder.query(db, projection, selection,
                    selectionArgs, groupBy, having, sortOrder);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);

        // Return the result Cursor.
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        // Return a string that identifies the MIME type
        // for a Content Provider URI
        switch (uriMatcher.match(uri)) {
            case ALL_EVENTS:
                return "vnd.android.cursor.dir/vnd.com.oktogen.emansampleapp.Events";
            case SINGLE_EVENTS:
                return "vnd.android.cursor.dir/vnd.com.oktogen.emansampleapp.Events";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
            }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = myOpenHelper.getWritableDatabase();

        switch (uriMatcher.match(uri)) {
            case SINGLE_EVENTS:
                String rowID = uri.getPathSegments().get(1);
                selection = ROW_ID + "=" + rowID + (!TextUtils.isEmpty(selection) ?  " AND (" + selection + ')' : "");
            default: break;
        }

        if (selection == null)
            selection = "1";

        int deleteCount = db.delete(getTableNameFromUri(uri),
                selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return deleteCount;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = myOpenHelper.getWritableDatabase();
        String nullColumnHack = null;

        long id = db.insert(getTableNameFromUri(uri), nullColumnHack, values);
        if (id > -1) {
            Uri insertedId = ContentUris.withAppendedId(getContentUriFromUri(uri), id);
                                getContext().getContentResolver().notifyChange(insertedId, null);
            getContext().getContentResolver().notifyChange(insertedId, null);
            return insertedId;
        } else {
            return null;
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // Open a read / write database to support the transaction.
        SQLiteDatabase db = myOpenHelper.getWritableDatabase();

        // If this is a row URI, limit the deletion to the specified row.
        switch (uriMatcher.match(uri)) {
            case SINGLE_EVENTS:
                String rowID = uri.getPathSegments().get(1);
                selection = ROW_ID + "=" + rowID + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
            default: break;
        }

        // Perform the update.
        int updateCount = db.update(getTableNameFromUri(uri), values, selection, selectionArgs);
        // Notify any observers of the change in the data set.
        getContext().getContentResolver().notifyChange(uri, null);
        return updateCount;
    }

    protected static class DbHelper extends SQLiteOpenHelper {
        public DbHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        // Called when no database exists in disk and the helper class needs
        // to create a new one. 
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_EVENTS_CREATE);
        }

        // Called when there is a database version mismatch meaning that the version
        // of the database on disk needs to be upgraded to the current version.
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Log the version upgrade.
            Log.w(TAG, "Upgrading from version " + 
                        oldVersion + " to " +
                        newVersion + ", which will destroy all old data");
            
            // Upgrade the existing database to conform to the new version. Multiple 
            // previous versions can be handled by comparing _oldVersion and _newVersion
            // values.

            // The simplest case is to drop the old table and create a new one.
            db.execSQL("DROP TABLE IF EXISTS " + EVENTS_TABLE + "");
            // Create a new one.
            onCreate(db);
        }
    }

    /*==============================================================================*/
	/*Custom bulk insert*/
	/*==============================================================================*/


    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {

        final SQLiteDatabase db = myOpenHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);

        int numInserted = 0;

        switch (match) {
            case ALL_EVENTS:
                Logg.e(TAG, "EVENTS + count: " + values.length);
                db.beginTransaction();
                try {
                    batchInsert(uri, db, values);
                    db.setTransactionSuccessful();
                    //getContext().getContentResolver().notifyChange(uri, null);
                    getContext().getContentResolver().notifyChange(EVENTS_URI, null);
                    numInserted = values.length;
                }catch(Exception e){
                    e.printStackTrace();
                }finally{
                    db.endTransaction();
                }
                return numInserted;
            default:
                throw new UnsupportedOperationException("unsupported uri: "+uri);

        }

    }

    private void batchInsert(Uri uri, SQLiteDatabase db, ContentValues[] values) {
        for(ContentValues cv : values) {
            int updated  = db.update(getTableNameFromUri(uri), cv, DataProvider.EVENTS_EVENTID_COLUMN+" = " + cv.getAsInteger(DataProvider.EVENTS_EVENTID_COLUMN), null);
            if(updated == 0) {
                db.insert(getTableNameFromUri(uri), null, cv);
            }
        }
    }
}