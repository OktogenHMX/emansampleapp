/**********************************************************************************************************************************************************************
****** AUTO GENERATED FILE BY ANDROID SQLITE HELPER SCRIPT BY FEDERICO PAOLINELLI. ANY CHANGE WILL BE WIPED OUT IF THE SCRIPT IS PROCESSED AGAIN. *******
**********************************************************************************************************************************************************************/
package com.oktogen.emansampleapp.provider;


import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.oktogen.emansampleapp.utils.Logg;

import java.util.Date;

public class DataProviderClient{


    // ------------- EVENTS_HELPERS ------------
    public static Uri addEvents (Integer eventId, 
                                Integer eventScheduleId, 
                                String lastModified, 
                                String eventName, 
                                String eventDetailUrl, 
                                String webDescription, 
                                String state, 
                                Integer filmRating, 
                                String criticName, 
                                String category, 
                                Integer timesPick, 
                                Integer free, 
                                Integer kidFriendly, 
                                Integer lastChance, 
                                Integer festival, 
                                Integer longRunningShow, 
                                Integer previewsAndOpenings, 
                                String recurringStartDate, 
                                Context c) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataProvider.EVENTS_EVENTID_COLUMN, eventId);
        contentValues.put(DataProvider.EVENTS_EVENTSCHEDULEID_COLUMN, eventScheduleId);
        contentValues.put(DataProvider.EVENTS_LASTMODIFIED_COLUMN, lastModified);
        contentValues.put(DataProvider.EVENTS_EVENTNAME_COLUMN, eventName);
        contentValues.put(DataProvider.EVENTS_EVENTDETAILURL_COLUMN, eventDetailUrl);
        contentValues.put(DataProvider.EVENTS_WEBDESCRIPTION_COLUMN, webDescription);
        contentValues.put(DataProvider.EVENTS_STATE_COLUMN, state);
        contentValues.put(DataProvider.EVENTS_FILMRATING_COLUMN, filmRating);
        contentValues.put(DataProvider.EVENTS_CRITICNAME_COLUMN, criticName);
        contentValues.put(DataProvider.EVENTS_CATEGORY_COLUMN, category);
        contentValues.put(DataProvider.EVENTS_TIMESPICK_COLUMN, timesPick);
        contentValues.put(DataProvider.EVENTS_FREE_COLUMN, free);
        contentValues.put(DataProvider.EVENTS_KIDFRIENDLY_COLUMN, kidFriendly);
        contentValues.put(DataProvider.EVENTS_LASTCHANCE_COLUMN, lastChance);
        contentValues.put(DataProvider.EVENTS_FESTIVAL_COLUMN, festival);
        contentValues.put(DataProvider.EVENTS_LONGRUNNINGSHOW_COLUMN, longRunningShow);
        contentValues.put(DataProvider.EVENTS_PREVIEWSANDOPENINGS_COLUMN, previewsAndOpenings);
        contentValues.put(DataProvider.EVENTS_RECURRINGSTARTDATE_COLUMN, recurringStartDate);
        ContentResolver cr = c.getContentResolver();
        return cr.insert(DataProvider.EVENTS_URI, contentValues);
    }

    public static int removeEvents(long rowIndex, Context c){
        ContentResolver cr = c.getContentResolver();
        Uri rowAddress = ContentUris.withAppendedId(DataProvider.EVENTS_URI, rowIndex);
        return cr.delete(rowAddress, null, null);
    }

    public static int removeAllEvents(Context c){
        ContentResolver cr = c.getContentResolver();
        return cr.delete(DataProvider.EVENTS_URI, null, null);
    }

    public static Cursor getAllEvents(Context c){
    	ContentResolver cr = c.getContentResolver();
        String[] resultColumns = new String[] {
                         DataProvider.ROW_ID,
                         DataProvider.EVENTS_EVENTID_COLUMN,
                         DataProvider.EVENTS_EVENTSCHEDULEID_COLUMN,
                         DataProvider.EVENTS_LASTMODIFIED_COLUMN,
                         DataProvider.EVENTS_EVENTNAME_COLUMN,
                         DataProvider.EVENTS_EVENTDETAILURL_COLUMN,
                         DataProvider.EVENTS_WEBDESCRIPTION_COLUMN,
                         DataProvider.EVENTS_STATE_COLUMN,
                         DataProvider.EVENTS_FILMRATING_COLUMN,
                         DataProvider.EVENTS_CRITICNAME_COLUMN,
                         DataProvider.EVENTS_CATEGORY_COLUMN,
                         DataProvider.EVENTS_TIMESPICK_COLUMN,
                         DataProvider.EVENTS_FREE_COLUMN,
                         DataProvider.EVENTS_KIDFRIENDLY_COLUMN,
                         DataProvider.EVENTS_LASTCHANCE_COLUMN,
                         DataProvider.EVENTS_FESTIVAL_COLUMN,
                         DataProvider.EVENTS_LONGRUNNINGSHOW_COLUMN,
                         DataProvider.EVENTS_PREVIEWSANDOPENINGS_COLUMN,
                         DataProvider.EVENTS_RECURRINGSTARTDATE_COLUMN
                         };

        Cursor resultCursor = cr.query(DataProvider.EVENTS_URI, resultColumns, null, null, null);
        return resultCursor;
    }

    public static Cursor getEvents(long rowId, Context c){
    	ContentResolver cr = c.getContentResolver();
        String[] resultColumns = new String[] {
                         DataProvider.ROW_ID,
                         DataProvider.EVENTS_EVENTID_COLUMN,
                         DataProvider.EVENTS_EVENTSCHEDULEID_COLUMN,
                         DataProvider.EVENTS_LASTMODIFIED_COLUMN,
                         DataProvider.EVENTS_EVENTNAME_COLUMN,
                         DataProvider.EVENTS_EVENTDETAILURL_COLUMN,
                         DataProvider.EVENTS_WEBDESCRIPTION_COLUMN,
                         DataProvider.EVENTS_STATE_COLUMN,
                         DataProvider.EVENTS_FILMRATING_COLUMN,
                         DataProvider.EVENTS_CRITICNAME_COLUMN,
                         DataProvider.EVENTS_CATEGORY_COLUMN,
                         DataProvider.EVENTS_TIMESPICK_COLUMN,
                         DataProvider.EVENTS_FREE_COLUMN,
                         DataProvider.EVENTS_KIDFRIENDLY_COLUMN,
                         DataProvider.EVENTS_LASTCHANCE_COLUMN,
                         DataProvider.EVENTS_FESTIVAL_COLUMN,
                         DataProvider.EVENTS_LONGRUNNINGSHOW_COLUMN,
                         DataProvider.EVENTS_PREVIEWSANDOPENINGS_COLUMN,
                         DataProvider.EVENTS_RECURRINGSTARTDATE_COLUMN
                         };

        Uri rowAddress = ContentUris.withAppendedId(DataProvider.EVENTS_URI, rowId);
        String where = null;    
        String whereArgs[] = null;
        String order = null;
    
        Cursor resultCursor = cr.query(rowAddress, resultColumns, where, whereArgs, order);
        return resultCursor;
    }

    public static int updateEvents (int rowId, 
                                   Integer eventId,
                                   Integer eventScheduleId,
                                   String lastModified,
                                   String eventName,
                                   String eventDetailUrl,
                                   String webDescription,
                                   String state,
                                   Integer filmRating,
                                   String criticName,
                                   String category,
                                   Integer timesPick,
                                   Integer free,
                                   Integer kidFriendly,
                                   Integer lastChance,
                                   Integer festival,
                                   Integer longRunningShow,
                                   Integer previewsAndOpenings,
                                   String recurringStartDate,
                                   Context c) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataProvider.EVENTS_EVENTID_COLUMN, eventId);
        contentValues.put(DataProvider.EVENTS_EVENTSCHEDULEID_COLUMN, eventScheduleId);
        contentValues.put(DataProvider.EVENTS_LASTMODIFIED_COLUMN, lastModified);
        contentValues.put(DataProvider.EVENTS_EVENTNAME_COLUMN, eventName);
        contentValues.put(DataProvider.EVENTS_EVENTDETAILURL_COLUMN, eventDetailUrl);
        contentValues.put(DataProvider.EVENTS_WEBDESCRIPTION_COLUMN, webDescription);
        contentValues.put(DataProvider.EVENTS_STATE_COLUMN, state);
        contentValues.put(DataProvider.EVENTS_FILMRATING_COLUMN, filmRating);
        contentValues.put(DataProvider.EVENTS_CRITICNAME_COLUMN, criticName);
        contentValues.put(DataProvider.EVENTS_CATEGORY_COLUMN, category);
        contentValues.put(DataProvider.EVENTS_TIMESPICK_COLUMN, timesPick);
        contentValues.put(DataProvider.EVENTS_FREE_COLUMN, free);
        contentValues.put(DataProvider.EVENTS_KIDFRIENDLY_COLUMN, kidFriendly);
        contentValues.put(DataProvider.EVENTS_LASTCHANCE_COLUMN, lastChance);
        contentValues.put(DataProvider.EVENTS_FESTIVAL_COLUMN, festival);
        contentValues.put(DataProvider.EVENTS_LONGRUNNINGSHOW_COLUMN, longRunningShow);
        contentValues.put(DataProvider.EVENTS_PREVIEWSANDOPENINGS_COLUMN, previewsAndOpenings);
        contentValues.put(DataProvider.EVENTS_RECURRINGSTARTDATE_COLUMN, recurringStartDate);
        Uri rowAddress = ContentUris.withAppendedId(DataProvider.EVENTS_URI, rowId);

        ContentResolver cr = c.getContentResolver();
        int updatedRowCount = cr.update(rowAddress, contentValues, null, null);
        return updatedRowCount;
    }

    public static synchronized int bulkInsertAppData(Uri uri, ContentValues[] contentValues, Context c) {
        if(contentValues ==  null)  {
            Logg.e("", "content values is null");
            return -1;
        }
        ContentResolver cr = c.getContentResolver();
        return cr.bulkInsert(uri, contentValues);
    }
}
