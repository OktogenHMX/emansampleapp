package com.oktogen.emansampleapp.utils;

import android.content.Context;
import android.content.res.Configuration;

import com.oktogen.emansampleapp.base.Constants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Created by oktogen on 4.7.15.
 */
public class CommonMethods {
    private static final String TAG = "CommonMethods";

    public static String getRequestUrl(int limit, int offset){
        StringBuilder sb = new StringBuilder();

        sb.append(Constants.baseUrl);

        sb.append(Constants.paramLimit);
        sb.append(limit);
        sb.append("&");

        sb.append(Constants.paramOffset);
        sb.append(offset);
        sb.append("&");

        sb.append(Constants.paramApiKey);
        sb.append(Constants.apiKey);

        return sb.toString();
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
