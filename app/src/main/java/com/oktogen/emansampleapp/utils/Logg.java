package com.oktogen.emansampleapp.utils;

import android.util.Log;

import com.oktogen.emansampleapp.base.Constants;

public class Logg {

	public static final int VERBOSE = 1;
	public static final int DEBUG = 2;
	public static final int INFO = 3;
	public static final int WARNING = 4;
	public static final int ERROR = 5;
	
	public static final int ON = 0;
	public static final int OFF = 100;
	
	//public static int level = ON; 

	public static void v(String tag, String string) {
		if (getLevel() <= VERBOSE) {
			Log.v(tag, string);
		}
	}

	public static void d(String tag, String string) {
		if (getLevel() <= DEBUG) {
			Log.d(tag, string);
		}
	}

	public static void i(String tag, String string) {
		if (getLevel() <= INFO) {
			Log.i(tag, string);
		}
	}

	public static void w(String tag, String string) {
		if (getLevel() <= WARNING) {
			Log.w(tag, string);
		}
	}

	public static void e(String tag, String string) {
		if (getLevel() <= ERROR) {
			Log.e(tag, string);
		}
	}
	
	public static boolean isInDebugMode(){
		if (getLevel() == ON) {
			return true;
		}else{
			return false;
		}
	}
	
	private static int getLevel(){
		if(Constants.isLoggEnabled){
			return ON;
		}else{
			return OFF;
		}
	}
}
